package ru.nsu.fit.kazak.dis.lab2.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kazak.dis.lab2.Application;
import ru.nsu.fit.kazak.dis.lab2.exceptions.AuthException;

import java.sql.SQLException;
import java.sql.Savepoint;

public class Initializer {
    private static final Logger logger = LogManager.getLogger(Application.class);
    private final Connector connector;

    public Initializer(Connector connector) {
        this.connector = connector;
    }

    public void initDB() throws SQLException, AuthException {
        if (!connector.isAuthorised()) {
            throw new AuthException("You doesn't authorise");
        }
        Savepoint savepoint = null;
        connector.disableAutoCommit();
        try {
            savepoint = connector.setSavepoint();
            dropDB();
            createTables();
            connector.commit();
            logger.info("The data base is initialized successfully");
        } catch (SQLException exception) {
            try {
                if (savepoint != null) {
                    connector.rollBack(savepoint);
                }
                throw exception;
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
        // connector.enableAutoCommit();
    }

    private void dropDB() throws AuthException, SQLException {
        if (!connector.isAuthorised()) {
            throw new AuthException("You doesn't authorise");
        }
        Savepoint savepoint = null;
        try {
            savepoint = connector.setSavepoint();
            dropTables();
            connector.commit();
            logger.info("The data base is dropped successfully");
        } catch (SQLException exception) {
            try {
                if (savepoint != null) {
                    connector.rollBack(savepoint);
                }
                throw exception;
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    private void createTables() throws SQLException {
        connector.execPreparedStatementUpdate("CREATE TABLE NODES(id SERIAL PRIMARY KEY,tags integer[],lat numeric(7),long numeric(7),username text,changeset text)");
        connector.execPreparedStatementUpdate("CREATE TABLE RELATION (id SERIAL PRIMARY KEY,members integer[],tags integer[])");
        connector.execPreparedStatementUpdate("CREATE TABLE MEMBER (id SERIAL PRIMARY KEY,type text,ref integer[])");
        connector.execPreparedStatementUpdate("CREATE TABLE TAGS(id integer PRIMARY KEY,key text,value text)");
    }

    private void dropTables() throws SQLException {
        connector.execPreparedStatementUpdate("DROP TABLE NODES");
        connector.execPreparedStatementUpdate("DROP TABLE RELATION");
        connector.execPreparedStatementUpdate("DROP TABLE MEMBER");
        connector.execPreparedStatementUpdate("DROP TABLE TAGS");
    }
}
