package ru.nsu.fit.kazak.dis.lab2.database.dao;

import java.util.List;

public record Node(int id, List<Integer> tags, double latitude, double longitude, String username, String changeset) {
}
