package ru.nsu.fit.kazak.dis.lab2.database.dao;

import ru.nsu.fit.kazak.dis.lab2.database.Connector;


public abstract class AbstractController <E> {
    protected final Connector connector;

    protected AbstractController(Connector connector) {
        this.connector = connector;
    }
}
