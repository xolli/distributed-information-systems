package ru.nsu.fit.kazak.dis.lab2.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openstreetmap.osm._0.Node;
import ru.nsu.fit.kazak.dis.lab2.database.dao.NodeController;
import ru.nsu.fit.kazak.dis.lab2.database.dao.TagController;

public class DatabaseNodeIterator {
    private static final Logger logger = LogManager.getLogger(DatabaseNodeIterator.class);
    private final NodeController nodeController;
    private final TagController tagController;
    private final Connector connector;

    public DatabaseNodeIterator(Connector connector) {
        this.nodeController = new NodeController(connector);
        this.tagController = new TagController(connector);
        this.connector = connector;
    }

    public void iterate(Node xmlNode) {
        logger.info(xmlNode);
    }
}
