package ru.nsu.fit.kazak.dis.lab2;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import java.io.*;

public class Decompresser {
    private static int BUFFER_SIZE = 4098;

    // https://stackoverflow.com/questions/4834721/java-read-bz2-file-and-uncompress-parse-on-the-fly
    public static BufferedReader getBufferedReaderForCompressedFile(String fileIn) throws FileNotFoundException, CompressorException {
        FileInputStream fin = new FileInputStream(fileIn);
        BufferedInputStream bis = new BufferedInputStream(fin, BUFFER_SIZE);
        CompressorInputStream input = new CompressorStreamFactory().createCompressorInputStream(bis);
        return new BufferedReader(new InputStreamReader(input));
    }
}
