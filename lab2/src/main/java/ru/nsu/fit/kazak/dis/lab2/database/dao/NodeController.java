package ru.nsu.fit.kazak.dis.lab2.database.dao;

import ru.nsu.fit.kazak.dis.lab2.database.Connector;

import java.sql.SQLException;
import java.util.List;

public class NodeController extends AbstractController<Node> {

    private int countOfNodes;

    public void create(List<Integer> tagsId, Double lat, Double lon, String user, String changeset) throws SQLException {
        String queryText = constructInsert(tagsId, lat, lon, user, changeset);
        connector.execQueryStatementUpdate(queryText);
    }

    public String constructInsert(List<Integer> tagsId, Double lat, Double lon, String user, String changeset) {
        ++countOfNodes;
        return String.format("INSERT INTO NODES(tags, lat, long, username, changeset) VALUES (ARRAY [%s]::integer[], %.7f, %.7f, $$%s$$, $$%s$$)",
                tagsToString(tagsId), lat, lon, user, changeset);
    }

    public NodeController(Connector connector) {
        super(connector);
        countOfNodes = 0;
    }

    private String tagsToString(List<Integer> tags) {
        if (tags.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (var tag : tags) {
            sb.append(tag);
            sb.append(",");
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public int getCountOfNodes() {
        return countOfNodes;
    }
}
