package ru.nsu.fit.kazak.dis.lab2.database;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openstreetmap.osm._0.Node;
import org.openstreetmap.osm._0.Tag;
import org.xml.sax.SAXException;
import ru.nsu.fit.kazak.dis.lab2.NodeUnmarshaller;
import ru.nsu.fit.kazak.dis.lab2.StatResult;
import ru.nsu.fit.kazak.dis.lab2.database.dao.NodeController;
import ru.nsu.fit.kazak.dis.lab2.database.dao.TagController;
import ru.nsu.fit.kazak.dis.lab2.exceptions.UnknownIdException;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DatabaseUnmarshaller {
    private static final Logger logger = LogManager.getLogger(DatabaseUnmarshaller.class);

    private final NodeController nodeController;
    private final TagController tagController;
    private final Connector connector;

    public DatabaseUnmarshaller(Connector connector) {
        this.nodeController = new NodeController(connector);
        this.tagController = new TagController(connector);
        this.connector = connector;
    }

    public void insertNodesOneBatch(Reader xmlReader) throws XMLStreamException, JAXBException, SAXException, SQLException {
        var unmarshaller = new NodeUnmarshaller();
        var nodes = unmarshaller.getAllNodes(xmlReader);
        logger.info("Nodes have been read");
        Savepoint savepoint = null;
        try (var nodeStatement = connector.createStatement();
            var tagStatement = connector.createStatement()) {
            savepoint = connector.setSavepoint();
            for (var xmlNode : nodes) {
                var tagsId = insertTags(xmlNode.getTag(), tagStatement);
                nodeStatement.addBatch(nodeController.constructInsert(tagsId, xmlNode.getLat(), xmlNode.getLon(), xmlNode.getUser(), xmlNode.getChangeset().toString()));
            }
            tagStatement.executeBatch();
            nodeStatement.executeBatch();
            connector.commit();
            logger.info("Nodes was inserted");
        } catch (SQLException exception) {
            try {
                if (savepoint != null) {
                    connector.rollBack(savepoint);
                }
                throw exception;
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    public void psBatchInsertNodes(Reader xmlReader) throws XMLStreamException, JAXBException, SAXException, SQLException {
        var unmarshaller = new NodeUnmarshaller();
        var nodes = unmarshaller.getAllNodes(xmlReader);
        logger.info("Nodes have been read");
        Savepoint savepoint = null;
        try (var nodeStatement = connector.createPrepareStatement("INSERT INTO NODES(tags, lat, long, username, changeset) VALUES (?, ?, ?, ?, ?)");
            var tagStatement = connector.createPrepareStatement("INSERT INTO TAGS VALUES (?, ?, ?)")) {
            savepoint = connector.setSavepoint();
            int count = 0;
            for (var xmlNode : nodes) {
                var tagsId = insertTagsPsAndBatch(xmlNode.getTag(), tagStatement);
                insertNodePs(tagsId, xmlNode, nodeStatement);
                nodeStatement.addBatch();
                if (++count >= 1000) {
                    count = 0;
                    nodeStatement.executeBatch();
                    tagStatement.executeBatch();
                }
            }
            tagStatement.executeBatch();
            nodeStatement.executeBatch();
            connector.commit();
            logger.info("Nodes was inserted");
        } catch (SQLException exception) {
            try {
                if (savepoint != null) {
                    connector.rollBack(savepoint);
                }
                throw exception;
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    public void psInsertNodes(Reader xmlReader) throws XMLStreamException, JAXBException, SAXException, SQLException {
        var unmarshaller = new NodeUnmarshaller();
        var nodes = unmarshaller.getAllNodes(xmlReader);
        logger.info("Nodes have been read");
        Savepoint savepoint = null;
        try (var nodeStatement = connector.createPrepareStatement("INSERT INTO NODES(tags, lat, long, username, changeset) VALUES (?, ?, ?, ?, ?)");
            var tagStatement = connector.createPrepareStatement("INSERT INTO TAGS VALUES (?, ?, ?)")) {
            savepoint = connector.setSavepoint();
            for (var xmlNode : nodes) {
                var tagsId = insertTagsPsAndExec(xmlNode.getTag(), tagStatement);
                insertNodePs(tagsId, xmlNode, nodeStatement);
                nodeStatement.executeUpdate();
            }
            connector.commit();
            logger.info("Nodes was inserted");
        } catch (SQLException exception) {
            try {
                if (savepoint != null) {
                    connector.rollBack(savepoint);
                }
                throw exception;
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }

    private List<Integer> insertTagsPsAndExec(List<Tag> xmlTags, PreparedStatement tagStatement) throws SQLException {
        List<Integer> tagsId = new ArrayList<>();
        for (var xmlTag : xmlTags) {
            int tagId = tagController.getNextId();
            tagStatement.setInt(1, tagId);
            tagStatement.setString(2, xmlTag.getK());
            tagStatement.setString(3, xmlTag.getV());
            tagsId.add(tagId);
            tagStatement.executeUpdate();
        }
        return tagsId;
    }

    private void insertNodePs(List<Integer> tagsId, Node xmlNode, PreparedStatement nodeStatement) throws SQLException {
        nodeStatement.setArray(1, connector.createIntArray(tagsId));
        nodeStatement.setBigDecimal(2, BigDecimal.valueOf(xmlNode.getLat()));
        nodeStatement.setBigDecimal(3, BigDecimal.valueOf(xmlNode.getLon()));
        nodeStatement.setString(4, xmlNode.getUser());
        nodeStatement.setString(5, xmlNode.getChangeset().toString());
    }

    private List<Integer> insertTagsPsAndBatch(List<Tag> xmlTags, PreparedStatement tagStatement) throws SQLException {
        List<Integer> tagsId = new ArrayList<>();
        for (var xmlTag : xmlTags) {
            int tagId = tagController.getNextId();
            tagStatement.setInt(1, tagId);
            tagStatement.setString(2, xmlTag.getK());
            tagStatement.setString(3, xmlTag.getV());
            tagsId.add(tagId);
            tagStatement.addBatch();
        }
        return tagsId;
    }

    public StatResult calcStat() throws SQLException {
        try (var result = connector.getRequest(
                "SELECT username, COUNT(DISTINCT changeset) as cnt_changes FROM NODES GROUP BY username ORDER BY cnt_changes DESC")) {
            logger.info("User statistic was calculated");
            List<Pair<String, Integer>> userStats = new ArrayList<>();
            while (result.next()) {
                var username = result.getString(1);
                var countOfChanges = result.getInt(2);
                userStats.add(Pair.of(username, countOfChanges));
            }
            try {
                return new StatResult(userStats, countNodesWithName());
            } catch (UnknownIdException ex) {
                throw new SQLException(ex);
            }
        }
    }

    private int countNodesWithName() throws SQLException, UnknownIdException {
        try (var result = connector.getRequest("SELECT tags FROM NODES WHERE tags <> '{}'")) {
            logger.info("Nodes without tags were selected");
            int countNodes = 0;
            while (result.next()) {
                var tagsId = arrayToListInteger(result.getArray(1));
                countNodes += tagContainsName(tagsId) ? 1 : 0;
            }
            logger.info("Nodes with names were counted");
            return countNodes;
        }
    }

    private boolean tagContainsName(List<Integer> tagsId) throws SQLException, UnknownIdException {
        for (var tagId : tagsId) {
            try (var tag = connector.getRequest("SELECT key FROM TAGS WHERE id = " + tagId)) {
                if (!tag.next()) {
                    throw new UnknownIdException("Unknowd tag id: " + tagId);
                }
                var key = tag.getString(1);
                if (key.equals("name")) {
                    return true;
                }
            }
        }
        return false;
    }

    private List<Integer> arrayToListInteger(Array arrayInt) throws SQLException {
        Integer[] numbers = (Integer[]) arrayInt.getArray();
        return new ArrayList<>(Arrays.asList(numbers));
    }

    private List<Integer> insertTags(List<Tag> xmlTags, Statement tagStatement) throws SQLException {
        List<Integer> tagsId = new ArrayList<>();
        for (var xmlTag : xmlTags) {
            tagStatement.addBatch(tagController.constructInsert(xmlTag.getK(), xmlTag.getV()));
            tagsId.add(tagController.getCurrentId());
        }
        return tagsId;
    }
}
