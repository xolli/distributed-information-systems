package ru.nsu.fit.kazak.dis.lab2;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import ru.nsu.fit.kazak.dis.lab2.database.Connector;
import ru.nsu.fit.kazak.dis.lab2.database.DatabaseUnmarshaller;
import ru.nsu.fit.kazak.dis.lab2.database.Initializer;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.Reader;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);
    private static final String TOOL_NAME = "osmstats";

    public static void main(String[] args) {
        Options options = initOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        try {
            CommandLine cmd = parser.parse(options, args);
            try (BufferedReader reader = Decompresser.getBufferedReaderForCompressedFile(cmd.getOptionValue("input"));
                 Connector connector = new Connector("osmadmin", "m7dkvk4c", "localhost", 5432)) {
                /*Initializer initializer = new Initializer(connector);
                initializer.initDB();
                var unmarshaller = new DatabaseUnmarshaller(connector);
                unmarshaller.psInsertNodes(reader);
                var stat = unmarshaller.calcStat();
                for (var entry : stat.getUserStats()) {
                    logger.info(entry);
                }
                logger.info("Count of nodes with names = {}", stat.getNodeWithNamesCount());*/
                iterateByNodes(reader);
            }
        } catch (ParseException e) {
            logger.error(e.getMessage());
            formatter.printHelp(TOOL_NAME, options);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private static void iterateByNodes(Reader xmlReader) throws XMLStreamException, JAXBException, SAXException {
        var unmarshaller = new NodeUnmarshaller();
        unmarshaller.iterateByNode(xmlReader);
    }

    private static Options initOptions() {
        Options options = new Options();

        Option yesterdayPages = new Option("i", "input", true, "input file xml.bz2 format with OSM data");
        yesterdayPages.setRequired(true);
        options.addOption(yesterdayPages);

        return options;
    }

}
