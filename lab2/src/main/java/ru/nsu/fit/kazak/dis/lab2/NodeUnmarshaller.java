package ru.nsu.fit.kazak.dis.lab2;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.openstreetmap.osm._0.Node;
import org.openstreetmap.osm._0.Osm;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.util.StreamReaderDelegate;
import javax.xml.transform.sax.SAXSource;
import java.io.Reader;
import java.math.BigInteger;
import java.util.*;

import org.apache.commons.lang3.tuple.Pair;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class NodeUnmarshaller {
    public List<Node> getAllNodes(Reader xmlReader) throws JAXBException, XMLStreamException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(Osm.class);
        Unmarshaller u = jc.createUnmarshaller();
        XMLReader reader = XMLReaderFactory.createXMLReader();
        NamespaceFilter inFilter = new NamespaceFilter("http://openstreetmap.org/osm/0.6", true);
        inFilter.setParent(reader);
        InputSource is = new InputSource(xmlReader);
        SAXSource source = new SAXSource(inFilter, is);
        Osm osm = (Osm) u.unmarshal(source);
        return osm.getNode();
    }

    public void iterateByNode(Reader xmlReader) throws JAXBException, XMLStreamException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(Node.class);
        Unmarshaller u = jc.createUnmarshaller();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(xmlReader);
        while(xmlEventReader.hasNext()){
            var xmlEvent = xmlEventReader.peek();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                if (!startElement.getName().getLocalPart().equals("node")) {
                    xmlEventReader.next();
                    continue;
                }
                Node node = u.unmarshal(xmlEventReader, Node.class).getValue();
            } else {
                xmlEventReader.next();
            }

        }
    }

    public StatResult calcStat(List<Node> nodes) {
        Map<String, Set<BigInteger>> userStatMap = new HashMap<>();
        long countNodesWithNames = 0;
        for (var node : nodes) {
            countNodesWithNames += haveName(node) ? 1 : 0;
            String user = node.getUser();
            userStatMap.computeIfAbsent(user, u -> new HashSet<>());
            userStatMap.get(user).add(node.getChangeset());
        }
        List<Pair<String, Integer>> userStats = new ArrayList<>();
        for (var elem : userStatMap.entrySet()) {
            userStats.add(new ImmutablePair<>(elem.getKey(), elem.getValue().size()));
        }
        userStats.sort((elem1, elem2) -> elem2.getValue().compareTo(elem1.getValue()));
        return new StatResult(userStats, countNodesWithNames);
    }

    private boolean haveName(Node node) {
        for (var tag : node.getTag()) {
            if (tag.getK().equals("name")) {
                return true;
            }
        }
        return false;
    }

    private static class XsiTypeReader extends StreamReaderDelegate {

        public XsiTypeReader(XMLStreamReader reader) {
            super(reader);
        }

        @Override
        public String getAttributeNamespace(int arg0) {
            if("type".equals(getAttributeLocalName(arg0))) {
                return XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI;
            }
            return super.getAttributeNamespace(arg0);
        }

        @Override
        public String getAttributeValue(int arg0) {
            String n = getAttributeLocalName(arg0);
            if("type".equals(n)) {
                String v = super.getAttributeValue(arg0);
                return  "xs:"+ v;
            }
            return super.getAttributeValue(arg0);
        }

        @Override
        public NamespaceContext getNamespaceContext() {
            return new MyNamespaceContext(super.getNamespaceContext());
        }
    }

    private static class MyNamespaceContext implements NamespaceContext {
        public NamespaceContext _context;
        public MyNamespaceContext(NamespaceContext c){
            _context = c;
        }

        @Override
        public Iterator<String> getPrefixes(String namespaceURI) {
            return _context.getPrefixes(namespaceURI);
        }

        @Override
        public String getPrefix(String namespaceURI) {
            return _context.getPrefix(namespaceURI);
        }

        @Override
        public String getNamespaceURI(String prefix) {
            if("xs".equals(prefix)) {
                return  "http://www.w3.org/2001/XMLSchema";
            }
            return _context.getNamespaceURI(prefix);
        }
    }
}
