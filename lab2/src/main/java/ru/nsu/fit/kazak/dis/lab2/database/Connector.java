package ru.nsu.fit.kazak.dis.lab2.database;


import java.sql.*;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;

public class Connector implements AutoCloseable {
    private static final int LOGIN_TIMEOUT = 4;
    private Connection connection;
    private final AtomicBoolean authorised = new AtomicBoolean(false);

    public Connector(String username, String password, String host, int port) throws SQLException {
        DriverManager.registerDriver (new org.postgresql.Driver());
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);
        authorise(username, password, host, port);
        connection.setAutoCommit(false);
    }

    private void authorise(String username, String password, String host, int port) throws SQLException {
        String address = "jdbc:postgresql://" + host + ":" + port + "/postgres";
        DriverManager.setLoginTimeout(LOGIN_TIMEOUT);
        connection = DriverManager.getConnection(address, "osmadmin", "m7dkvk4c");
        authorised.set(true);
    }

    public ResultSet getRequest(String sql) throws SQLException {
        PreparedStatement preStatement = connection.prepareStatement(sql);
        return preStatement.executeQuery();
    }

    public int execInsert(String sqlInsert) throws SQLException {
        try (Statement preStatement = connection.createStatement()) {
            preStatement.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);
            var generatedKey = preStatement.getGeneratedKeys();
            if(generatedKey.next()){
               return generatedKey.getInt(1);
            } else {
                throw new SQLException("Cannot get auto increment id");
            }
        }
    }

    public void execPreparedStatementUpdate(String command) throws SQLException {
        try (PreparedStatement stat = connection.prepareStatement(command)) {
            stat.executeUpdate();
        }
    }

    public Statement createStatement() throws SQLException {
        return connection.createStatement();
    }

    public void execQueryStatementUpdate(String command) throws SQLException {
        try (Statement stat = connection.createStatement()) {
            stat.executeUpdate(command);
        }
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }

    public boolean isAuthorised() {
        return authorised.get();
    }

    public void disableAutoCommit() throws SQLException {
        connection.setAutoCommit(false);
    }

    public void enableAutoCommit() throws SQLException {
        connection.setAutoCommit(true);
    }

    public void commit() throws SQLException {
        connection.commit();
    }

    public void rollBack() throws SQLException {
        connection.rollback();
    }

    public void rollBack(Savepoint savepoint) throws SQLException {
        connection.rollback(savepoint);
    }

    public Savepoint setSavepoint() throws SQLException {
        return connection.setSavepoint();
    }

    public PreparedStatement createPrepareStatement(String s) throws SQLException {
        return connection.prepareStatement(s);
    }

    public Array createIntArray(List<Integer> intList) throws SQLException {
        return connection.createArrayOf("integer", intList.toArray());
    }
}
