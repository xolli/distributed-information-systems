package ru.nsu.fit.kazak.dis.lab2.exceptions;

public class UnknownIdException extends Exception {
    public UnknownIdException(String message) {
        super(message);
    }
}
