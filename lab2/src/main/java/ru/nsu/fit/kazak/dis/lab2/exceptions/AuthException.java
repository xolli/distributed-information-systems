package ru.nsu.fit.kazak.dis.lab2.exceptions;

public class AuthException extends Exception {
    public AuthException(String msg) {
        super(msg);
    }
}
