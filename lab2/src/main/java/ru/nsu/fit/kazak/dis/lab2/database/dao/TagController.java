package ru.nsu.fit.kazak.dis.lab2.database.dao;

import ru.nsu.fit.kazak.dis.lab2.database.Connector;

import java.sql.SQLException;

public class TagController extends AbstractController<Tag> {

    private int currentId;
    public TagController(Connector connector) {
        super(connector);
        currentId = 0;
    }

    public int create(String key, String value) throws SQLException {
        String queryText = constructInsert(key, value);
        connector.execPreparedStatementUpdate(queryText);
        return currentId;
    }

    public String constructInsert(String key, String value) {
        return String.format("INSERT INTO TAGS VALUES (%d, $$%s$$, $$%s$$)", ++currentId, key, value);
    }

    public int getCurrentId() {
        return currentId;
    }

    public void increaseId() {
        ++currentId;
    }

    public int getNextId() {
        return ++currentId;
    }
}
