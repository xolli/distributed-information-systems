package ru.nsu.fit.kazak.dis.lab2;

import java.util.List;
import org.apache.commons.lang3.tuple.Pair;


public class StatResult {
    private final List<Pair<String, Integer>> userStats;
    private final long nodeWithNamesCount;

    public StatResult(List<Pair<String, Integer>> userStats, long nodeWithNamesCount) {
        this.userStats = userStats;
        this.nodeWithNamesCount = nodeWithNamesCount;
    }

    public List<Pair<String, Integer>> getUserStats() {
        return userStats;
    }

    public long getNodeWithNamesCount() {
        return nodeWithNamesCount;
    }
}
