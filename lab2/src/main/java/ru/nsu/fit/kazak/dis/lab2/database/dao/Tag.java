package ru.nsu.fit.kazak.dis.lab2.database.dao;

public record Tag(int id, String key, String value) {
}
